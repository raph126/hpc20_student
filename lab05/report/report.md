# HPC - Convolution  (partie I)

# Introduction

L'objectif de ce laboratoire est d'analyser les performance d'un programme limité par la bande passante des transferts mémoire. Pour commencer, il nous a été demandé de lire certains chapitres du document "What Every Programmer Should Know About Memory" (https://people.freebsd.org/~lstewart/articles/cpumemory.pdf). J'ai fait un résumé, de ce que j'ai compris et j'ai retenu, pour chacun de ces chapitres.

Ensuite, il a fallu analyser trois fonctions d'un programme fourni à l'aide des outils vus dans les précédents laboratoires : *google benchmark*, *PERF* et *Valgrind*.

Le CPU est un Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz et ses caches sont:

- L1 Data 32 KiB (x2)
- L1 Instruction 32 KiB (x2)
- L2 Unified 256 KiB (x2)
- L3 Unified 4096 KiB (x1)

## What Every Programmer Should Know About Memory

### Chapitre 1.0: Introduction

Au début, les différentes parties d'un ordinateur (processeur, mémoire, stockage de masse et interfaces réseau) étaient beaucoup plus simple et ont été développés en même temps. Leur performance était plus ou moins similaire. Une fois la structure de base faite, les développeurs se sont concentrés sur certains sous-systèmes. Cela créa des goulots d'étranglements, car certains composants étaient plus performants que d'autres. Ceci était particulièrement vrai pour le stockage de masse et la mémoire qui se sont développés moins rapidement pour raison de coûts. 

Pour le premier, des solutions logicielles ont principalement traité le problème: l'OS garde les données utilisées (et très probablement utilisées) dans la mémoire principale (accessible plus vite). L'ajout de la cache aux dispositifs de stockage a demandé des modification pour les OS pour garantir l'intégralité des données et améliorer les performances.  

La seconde a demandé presque que des modifications matériel. Ces dernières ont pris la forme de:

- RAM hardware design (speed and parallelism)

- Memory controller designs

- CPU caches

- Direct memory access (DMA) for devices.

### Chapitre 2.0: Hardware de base de nos jours

Aujourd'hui, il est plus rentable d'utiliser plusieurs petits ordinateurs basiques connectés entre eux pour une tâche qu'un seul gros système ultra-performants et très coûteux (dans de rares cas, il serait mieux de prendre la seconde option). Ceci est dû qu'il beaucoup de matériel réseau performant et pas cher. 

ATTENTION: ce qui va suivre correspond à environ 90% du matériel existant de 2007

![image-20200407181223595](/home/oem/.config/Typora/typora-user-images/image-20200407181223595.png)

La figure ci-dessus montre la structure des chipsets des PCs avec le Northbridge et Southbridge (I/O bridge). Tous les processeurs (peuvent être plus que 2) sont connecté au Northbridge. Celui-ci contient le contrôleur de mémoire et son implémentation qui définie le type de RAM du système (DRAM, SDRAM, ...). S'il veut atteindre d'autres périphériques, il doit communiquer avec le Southbridge. Il assure la communication avec d'autres périphériques avec des bus PCI , PCI Epress, SATA, USB, ports PATA, série et parallèle. Cette structure a des conséquence :

- Toutes les données qui vont d'une CPU à une autre doivent passer par le Northbridge
- Pareil avec la RAM
- La RAM a un seul port
- Si une CPU doit communiquer avec un périphérique attaché au Southbridge, il doit passer par le Northbridge

Quelques goulots d'étranglement apparaissent au niveau de la RAM. Un premier est son accès pour les périphériques. A l'époque, ils devaient passer par le processeur pour y accéder. Via la la DMA (et le Northbridge), ils peuvent directement stocker et recevoir des données de la mémoire vive. **Attention**, cela crée des conflits sur la largeur de bande du Northbridge, car les demandes de la DMAs sont en concurrence avec la CPU (à prendre en compte). Un second est l'accès à la RAM depuis  le Northbridge. Les anciennes RAMs n'avaient qu'un seul  bus. Pour les plus récentes, on a 2 canaux (DDR2) mais on peut en avoir plus avec la FB-DRAM par exemple. Cela double la bande passante.

Cette bande passante reste limitée. C'est pour ça qu'il est important de programmer l'accès à la mémoire de manière à réduire au minimum les délais et gagner en performance (surtout si tous les cœurs et la DMA accèdent en même temps à la mémoire). 

![image-20200407191504743](/home/oem/.config/Typora/typora-user-images/image-20200407191504743.png)

Sur des systèmes plus chers, le Northbridge n'a pas le contrôleur mémoire. Il est plutôt connecté à plusieurs contrôleurs mémoire (voir image). Ceci permet d'avoir plus de bus pour accéder à la mémoire (donc plus de bande passante) et peut aussi supporter plus de RAM. La limitation principale de cette architecture est la bande passante interne du Northbridge (énorme chez Intel). Les pattern d'accès concurrent à la mémoire réduisent les délais en accédant simultanément à différentes banques de mémoire.

![image-20200407192436139](/home/oem/.config/Typora/typora-user-images/image-20200407192436139.png)

Un autre moyen de réduire le temps d'accès à la mémoire est de connecter cette dernière directement au processeur et de lui intégrer le contrôleur mémoire. Il y a autant de banques de mémoire que de cœurs. Sans de Northbridge, cela réduit par 4 (dans l'exemple) l'accès à la mémoire. 

Il y a des désavantages avec cette architecture. La machine doit toujours garantir que toute la mémoire est accessible pour tous les CPUs. Ce qui rend la mémoire plus uniforme  (NUMA, Non-Uniform Memory Architecture machine). Sur l'image, si la CPU1 veut accéder à la RAM connectée à la CPU4, elle doit traverser 2 interconnexion entre processeur. Chaque interconnexion a un coût. Sur cette exemple, chaque CPU n'est qu'à 2 interconnexions de toute la RAM. Sur des machines plus complexes, cela peut être bien plus grand. D'autres machines organisent leurs CPUs en nœuds: un groupe de CPUs a accès à la mémoire. Dans le nœuds, le facteur NUMA peut être très réduit voire même inexistant. Mais la communication entre nœuds est très coûteuse.

### Chapitre 2.2.5: Types de RAM

L'accès aux DRAM n'est pas un processus rapide comparé à la vitesse à laquelle le processeur peut accéder aux registre et aux caches. La RAM et le CPU n'ont pas la même fréquence de fonctionnement. Un stall d'un cycle d'horloge sur le bus mémoire entraînera un stall de plusieurs cycles pour le processeur (dépend du ratio entre les 2: si il fait 11:1 coups d'horloge, processeur:RAM, il perdra 11).

Les modules DRAM sont capables de lire à des débits de données élévés sans aucun stall et le bus mémoire peut être occupé à 100%. Mais, à part désigné dans ce sens, l'accès à la DRAM ne se fait pas séquentiellement. Elle ne va pas allouer des plages continues de mémoires, ce qui engendre préchargement (chaque nouvelle sélection doit être déjà préchargée avant d'être attribuée) et une nouvelle row address selection (RAS) doit être faite. Plus vite le préchargement est fait et la RAS petite, plus la pénalité sera petite. 

La pré-extraction hardware et software peut être utilisée pour réduire le temps du stall. Elle permet aussi d'enlever des conflits en décalant les opérations mémoire dans le temps, juste avant que les données soient vraiment nécessaires (une lecture et une écriture doivent être faite sur les mêmes emplacement dans le même cycle, on décale la lecture pour que les opérations ne soient pas effectués en même)

### Chapitre 3.0: CPU caches

En raison de coûts économiques,les performances de la RAM et de la fréquence des bus mémoire n'ont pas  évolué au même rythme que les processeurs. Mais ce n'est pas la seule raison. Le faite d'avoir une RAM très rapide mais petite (SRAM) n'améliore pas la vitesse de lecture à cause de sa taille et en comparaison à celle des disques durs. Une RAM plus lente mais plus grande sera plus efficace. Ceci est dû à la vitesse d'accès des supports de stockage qui est plus lente que celle des DRAM. On peut placer une petite quantité de SRAM et une plus grande de DRAM sous contrôle de l'OS ou de l'utilisateur. MAis ce n'est pas une solution viable car ce gain en mémoire rapide serait vite absorbé par la gestion des ressources (taille de mémoire SRAM variable entre les processeurs, coûts de synchronisation entre les différents programmes, ....). La SRAM est devenue une ressources matériel administrée directement par la CPU. Elle est utilisée pour faire des copies temporaires des données (une cache). 

***VOIR DANS THEORIE "Localité"***

La petite taille de la cache est au faite que tous les données ne seront pas utilisées en même temps. Il faut avoir un ensemble de bonnes stratégies pour savoir ce qu'il faut mettre dans la cache au bon moment. Des techniques permettent d'amener des données avant qu'elles soient nécessaires au processeur (*prefetching*).  Cela permet de supprimer une partie des pénalités d'accès mémoire. 

### Chapitre 3.1: Big picture

Lu mais pas eu le temps de le résumer pour le rendu

### Chapitre 3.2: Opération de la cache de haut niveau

Lu mais pas eu le temps de le résumer pour le rendu

### Chapitre 3.5: Facteurs de cache miss

Lu mais pas eu le temps de le résumer pour le rendu

## Laboratoire

Pour avoir une première idée des performances du code, il a fallu mesurer le temps d'exécution pour chacune des images fournies. Le tableau ci-dessous montre la moyenne de 10 exécutions du programme avec la commande *time*. *gprof* n'est pas utile dans le cas présent, car il faut lire et écrire des fichiers ce qui dépend de la machine et non du code

|     Image     | Taille de l'image de base | Temps de traitement [s] |
| :-----------: | :-----------------------: | :---------------------: |
| half-life.png |          72.9 ko          |          0.285          |
| medalion.png  |         110.6 ko          |          0.103          |
|   stars.png   |          36.5 Mo          |          5.304          |

Malgré l'apparence plus complexe de l'image "medalion.png" et sa taille un peu plus élevée, la détection de ses contours se fait plus rapidement. Une hypothèse sera qu'il y a moins de grandes surfaces pleines dans "medalion.png" que dans "half-life.png". 

### Google Benchmark

Les google benchmarks vont pouvoir montrer quelle fonction demande plus de temps pour s'exécuter. Le tableau suivant montre les gbench avec le prefetcher activé.

| Images        | Fonction         | Temps de traitement [us] | Itérations |
| :------------ | :--------------- | :----------------------: | :--------: |
| medalion.png  | rgb_to_grayscale |           2984           |    235     |
| half-life.png | rgb_to_grayscale |          10800           |     65     |
| stars.png     | rgb_to_grayscale |          88044           |     7      |
| medalion.png  | gaussian_filter  |           9087           |     77     |
| half-life.png | gaussian_filter  |          44961           |     16     |
| stars.png     | gaussian_filter  |         1485521          |     1      |
| medalion.png  | sobel_filter     |          15514           |     45     |
| half-life.png | sobel_filter     |          70742           |     10     |
| stars.png     | sobel_filter     |         2067079          |     1      |

Au vu du temps pris par l'image "stars.png", son nombre d'itérations doit se trouver plutôt vers 70'000 pour rgb_to_grayscale et 100'000 pour gaussian_filter et sobel_filter.

Voici le tableau avec le prefetcher désactivé:

| Images        | Fonction         | Temps de traitement [us] | Itérations |
| ------------- | ---------------- | :----------------------: | :--------: |
| medalion.png  | rgb_to_grayscale |           2982           |    235     |
| half-life.png | rgb_to_grayscale |          11310           |     65     |
| stars.png     | rgb_to_grayscale |          88131           |     7      |
| medalion.png  | gaussian_filter  |           9217           |     76     |
| half-life.png | gaussian_filter  |          45350           |     16     |
| stars.png     | gaussian_filter  |         1541268          |     1      |
| medalion.png  | sobel_filter     |          15371           |     42     |
| half-life.png | sobel_filter     |          76173           |     10     |
| stars.png     | sobel_filter     |         2060318          |     1      |

Avec les gbench, le prefetcher a une influence minime sur le temps d'exécution du programme (de l'ordre de quelques nanosecondes voire microsecondes). Il se peut qu'il y aie des optimisations à faire dans ce sens.

### Perf

J'ai exécuté la commande *perf* avec comme arguments *cpu-clock* et *faults* pour chacune des images. La première permet de voir quelle fonction demande le plus de cycles d'horloge (ici surtout à titre indicatif car nous nous concentrons sur la mémoire dans ce laboratoire) et la seconde de savoir quelle fonction a le plus de page faults. Un tableau récapitulatif est au début des sous-chapitres suivants. Il récapitule, en pourcent, le nombre total de cycles d'horloge et de fautes de page que chaque fonction a fait.

Ensuite, je suis allé voir dans le code désassemblé afin de repérer les possibles goulots d'étranglements.

Avant chaque nouvelle exécution du code, un `make clean` et un nouveau `make` ont été fait afin de réinitialiser les données dans la mémoire.

#### half-life.png

| Fonction         | cpu-clock     | faults      |
| ---------------- | ------------- | ----------- |
| rgb_to_grayscale | 4.80% (~48)   | 7.46% (~9)  |
| gaussian_filter  | 18.04% (~180) | 8.08% (~10) |
| sobel_filter     | 30.39% (~300) | 8.76% (~11) |

Sur environ 1'000 coups d'horloge, sobel_filter en demande le plus et aussi crée le plus de fautes de page sur les 120 comptabilisées. Les 3 fonctions font leur fautes de page au même endroit: quand elle doivent écrire en mémoire:

- sobel_filter et gaussian_filter à la ligne `res_img->data[y * res_img->width + x] = img->data[y * img->width + x];`, aussi pour la première `res_img->data[y * res_img->width + x] = sqrt(Gx*Gx + Gy*Gy) > SOBEL_BINARY_THRESHOLD ? UINT8_MAX : 0;`et pour la seconde `res_img->data[y * res_img->width + x] = pix_acc / gauss_ponderation;`

- rgb_to_grayscale à la ligne 

  ```
  result->data[y * result->width + x] =
          		FACTOR_R * img->data[index + R_OFFSET] +
                  FACTOR_G * img->data[index + G_OFFSET] +
                  FACTOR_B * img->data[index + B_OFFSET];
  ```

Ceci est certainement dû à une mauvaise gestion de la cache. Valgrind va pouvoir aider là-dessus

#### medalion.png

| Fonction         | cpu-clock    | faults     |
| ---------------- | ------------ | ---------- |
| rgb_to_grayscale | 5.11% (~18)  | 4.92% (~5) |
| gaussian_filter  | 10.51% (~37) | 6.73% (~7) |
| sobel_filter     | 17.90% (~63) | 5.97% (~6) |

Cette fois, le programme n'a demandé que 352 coups d'horloge mais 102 fautes de page, ce qui veut dire qu'il y a bien un problème avec les caches. Le tableau montre des pourcentages moins élevés. Ceci est certainement dû au fait qu'il y a eu moins d'opérations à faire. gaussian_filter crée le plus de fautes cette fois-ci. Les goulots d'étranglement sont aux mêmes endroits que pour l'image précédente.

#### stars.png

| Fonction         | cpu-clock      | faults       |
| ---------------- | -------------- | ------------ |
| rgb_to_grayscale | 1.73% (~364)   | 8.92% (~178) |
| gaussian_filter  | 28.89% (~6067) | 8.99% (~180) |
| sobel_filter     | 40.11% (~8423) | 8.93% (~179) |

Ici, il a fallu environ 21'000 cycles d'horloge et plus de 2'000 fautes de pages pour détecter les bords de l'image. Le code désassemblé montre les mêmes problèmes que précédemment.

### Valgrind

Pour chacune des images, il a fallu la commande`valgrind --tool=callgrind --simulate-cache=yes ./sobel images/image.png images/edge_image.png` en remplaçant "image" par leur nom réel. Les 3 rapports, renommés en "1_val_half-life.out", "2_val_medalion.out" et "3_val_stars.out", ont été ouverts à l'aide de kcachegrind. 

Pour chaque événement des captures d'écran qui suivent, les chiffres sont un pourcentage qui prend en compte toutes les fonctions du programme. 

#### half-life.png

Le rapport pour cette image s'appelle "1_val_half-life.out"

##### rgb_to_grayscale

![image-20200408213800924](/home/oem/.config/Typora/typora-user-images/image-20200408213800924.png)

Cette fonction représente plus de 32% des caches miss en lecture de la L3. Si on regarde dans le code, le goulot d'étranglement se trouve au même endroit que ce *perf* nous avait montré. Dans le code, l'appel de la fonction se fait directement après l'initialisation des variables `gs_img`, `gauss_img` et `res_img` de la fonction `edge_detection`. Pourtant, ces variables ne sont mises en cache. Il y a donc un problème de localité spatiale. La taille du fichier peut aussi poser problème, la cache n'étant pas une grande mémoire.

Les caches miss en écriture représentent 7.36% du total. Ce nombre est élevé et il est peut-être dû au problème de localité évoqué avant.

##### gaussian_filter

![image-20200408213353761](/home/oem/.config/Typora/typora-user-images/image-20200408213353761.png)

Les caches miss en lecture et écriture de L1 sont dus à 46.08% et 45.81%. Le goulet est aussi au même endroit que ce que `perf` avait montré. Étant une mémoire petite (32KiB), la L1 ne peut pas garder toute l'image. C'est ça qu'elle sera obligé d'aller chercher vers les autres caches. Mais cela reste un pourcentage très élevé. Il y a encore des caches miss au niveau de la L3 en lecture (7.90%) et écriture (7.54%). La localité temporelle n'est pas respectée, ici.

##### sobel_filter

![image-20200408222400225](/home/oem/.config/Typora/typora-user-images/image-20200408222400225.png)

Les valeurs des différents pourcentages des caches miss sont très proche de ceux de `gaussian_filter`. Les mêmes remarques de s'appliquent ici. Le goulot est encore au même endroit.

#### medalion.png

Le rapport pour cette image s'appelle "2_val_medalion.out"

##### rgb_to_grayscale

![image-20200408222720794](/home/oem/.config/Typora/typora-user-images/image-20200408222720794.png)

Les mêmes remarques de "half-life.png" s'applique à cette fonction. Une différence par contre est que des données ont été trouvées dans la L2 (15.29% du total). Il y a aussi le pourcentage de l'événement "Extraction des instructions" qui est très élevé comparé à avant (42.25% contre 5.75%)

##### gaussian_filter

![image-20200408223547544](/home/oem/.config/Typora/typora-user-images/image-20200408223547544.png)

Les mêmes remarques de "half-life.png" s'applique aussi à cette fonction. Par contre les caches miss en L1 sont ~20% plus élevés qu'avant mais il y en a moins au niveau de la L3 (~5% de moins)

##### sobel_filter

Il n'y a pas de donnée pour cette fonction. Valgrind n'a peut-être rien mesuré pour celle-ci ou a planté en cours d'exécution. La première solution serait bien, car, en plus des différentes remarques ajoutées en plus de celles de l'image précédente, cela expliquerait pourquoi le filtre serait plus rapide alors que l'image prend plus de mémoire.



#### stars.png

Le rapport pour cette image s'appelle "3_val_stars.out"

##### rgb_to_grayscale

![image-20200408225021884](/home/oem/.config/Typora/typora-user-images/image-20200408225021884.png)

##### gaussian_filter

![image-20200408224950661](/home/oem/.config/Typora/typora-user-images/image-20200408224950661.png)

##### sobel_filter

![image-20200408225103294](/home/oem/.config/Typora/typora-user-images/image-20200408225103294.png)



Pour les 3 fonctions, les pourcentages sont presque toujours les mêmes (plus ou moins 2% au maximum) que pour la première image malgré une différence de taille très conséquente (quelques KiB à quelques MiB).  Les goulots d'étranglement sont exactement les mêmes pour les 2 images. Les conclusions sont donc les mêmes. 

## Conclusion

Une meilleure utilisation de des caches permettrait d'optimiser le code. De plus la taille de l'image importe peut dans le pourcentage de cache miss. Par contre, sa complexité peut compter: avec des surfaces pleines, le traitement peut être plus lent ("half-life.png" vs "mendalion.png"). Dans notre cas, le *prefetching* ne changeait pas les performances du programme.

Ce laboratoire fut très intéressant car il m'a permis d'apprendre beaucoup de choses. Il m'a aussi permis d'approfondir l'utilisation des différents outils vus dans les précédents labos et de connaître kcachegrind qui facilite grandement la lecture des rapports de Valgrind. 