# HPC - Optimisations de compilation

## Introduction

Pour ce laboratoire, il a été demandé de créer 3 fonctions simples sur lesquelles il faut appliquer une optimisation faite par le programmeur dans un premier cas et, dans un second cas, utiliser les flags de compilations.

Pour les optimisations du compilateur, les flag `-O1`, `-O2` et `-O3` ont été activés chacun leur tour afin de voir les améliorations du code assembleur. Ensuite d'autres flags qui ne sont pas compris dans `-O3` ont été ajoutés, si ce dernier ne suffisait pas.  Puisque ces options regroupent un grand ensemble d'optimisations, il se peut qu'il n'aie pas besoin d'avoir des flags supplémentaires

## Travail demandé

### 1er exemple : boucle simple

Pour le premier exemple, l'optimisation regardée est de voir si il est possible d'aider une boucle `for` à faire son travail. Cette boucle additionne chaque entier de 1 à n, ce qui correspond à une suite arithmétique. 

#### Optimisation manuelle

Cette suite commence par 1, donc elle peut être résumée par l'équation: 
$$
n(n+1)/2
$$
Via cette transformation, la boucle a disparu et le code assembleur passe de 18 à 13 lignes et n'a plus de boucle. Cela devrait augmenter fortement les performances.

#### Optimisations du compilateur

Le flag `-O3` a été activé.  Il donne une code plus compliqué à comprendre qu'avec `-O2` ou `-O1` (l'assembleur est similaire avec ces 2 options). Il faudrait tester les performances afin de voir quelle option est effectivement la meilleure

Code avec `-O3` : https://godbolt.org/z/PVF-cn (utilise des constantes, déclarées à la fin de l'assembleur, qui doivent permettre de faciliter les calculs)

Code avec `-O1` : https://godbolt.org/z/WYK-Ck (ressemble au code assembleur sans optimisation mais a les données directement dans des registres, donc n'a pas besoin de déréférencer des pointers)

Le flag de `-O3` qui différencie les deux cas précédents est `-ftree-loop-vectorize`. Par contre, ce dernier a besoin de plusieurs flags de `-O1` pour fonctionner. Cela veut dire qu'il faut parfois activer plusieurs options de compilation afin d'obtenir une optimisation.

Code avec `-O1` et `-ftree-loop-vectorize` : https://godbolt.org/z/L8utFY

Le flag `-funroll-loops` peut être aussi utilisé avec `-O3` (ou la version équivalante) afin d'enlever la boucle et stocker les données en mémoire. Le code est plus long et peut-être plus performant : https://godbolt.org/z/Az7Qt-

### 2ème exemple :

Pour le deuxième exemple, j'ai fait une fonction qui permet de détecter le nombre le plus grand entre deux entiers. L'optimisation recherchée est la suppression des branchs dus au `if`.

#### Optimisation manuelle

En utilisant un opérateur ternaire, cela permet d'effectuer l'optimisation voulue au niveau du programmeur.

#### Optimisations du compilateur

Le flag `-O3` ne rajoute aucune amélioration que `-O1`. Le code assembleur étant sur 4 lignes sans branch, il me semble pas qu'il existe une manière d'optimiser encore plus.

Code : https://godbolt.org/z/gXMoCv

### 3ème exemple : 

Pour le troisième exemple, le but est d'aller chercher une donnée en mémoire. Pour l'illustre, la fonction de base prend un entier en entrée et retourne une lettre de l’alphabète écrite en dur dans le code (de `a` à `e`). Le choix se fait à l'aide d'un switch. Si l'entier passé est plus petit que `1` ou plus grand que le nombre de lettres, la fonction retourne `0`.

#### Optimisation manuelle

Pour cette partie, il a fallu enlever le switch et le remplacer par un tableau qui contient les données. Un test a lieu afin de voir si l'entier permet l'accès à une case du tableau (retourne `0` sinon). Le déréférencement d'un pointer (ou la lecture d'une case d'un tableau) est plus rapide que la comparaison effectue dans le switch. Cela se voit sur les codes assembleur: `chooseOneLetter2` est bien plus court que `chooseOneLetter1` et a moins de branch.

#### Optimisations du compilateur

Le flag `-O1` n'apporte aucune modification mais `-O2` reduit fortement le code. Avec ce dernier, un pattern a été trouvé. Explication: la lettre `A` correspond à `0`, `B` à `1`, `C` à `2`, etc... Le compilateur va sauver dans des emplacements mémoires successifs. Il suffit alors d'additionner l'entier donné en entré de la fonction à l'offset mémoire de `A` pour avoir la lettre désirée. Il faut encore vérifier que le nombre passé ne dépasse pas le nombre maximum de lettres moins 1 (`0` est pour `A` ) et

Code : https://godbolt.org/z/Afx65i

Cela est aussi valable avec un nombre plus grand de lettres : https://godbolt.org/z/3ZinMd

