

# HPC - Convolution  (partie II)

## Introduction

Pour ce laboratoire, il a été demandé d'optimiser les 3 fonctions (`rgb_to_grayscale`, `gaussian_filter`, `sobel_filter`) analysées précédemment en utilisant les outils de profiling et la théorie du cours. Nous n'avons pas le droit de modifier le code des autres parties du programme. 

Afin d'avoir un temps de référence, une machine de référence a été mise à disposition. Tous les temps présentés dans ce rapport sortent de cette machine. Elle a comme cache:

- L1: 32 KiB Data (x4)
- L1: 32 KiB Instructions (x4) 
- L2 Unified 256 KiB (x4)
- L3 Unified 46080 KiB (x4)

Pour les analyses de Perf ou Valgrind, c'est ma machine personnelle qui est utilisée. Elle dispose de:

- L1 Data 32 KiB (x2)
- L1 Instruction 32 KiB (x2)
- L2 Unified 256 KiB (x2)
- L3 Unified 4096 KiB (x1)

## Convolution – Optimisation du code source

### Temps de base sur le serveur

Afin d'avoir un temps sur lequel se baser, j'ai lancé le programme sur le serveur sans modification.

| Images        | Fonction         | Temps de traitement [us] | Itérations |
| ------------- | ---------------- | :----------------------: | :--------: |
| medalion.png  | rgb_to_grayscale |          3'362           |    208     |
| half-life.png | rgb_to_grayscale |          12'083          |     58     |
| stars.png     | rgb_to_grayscale |          98'780          |     6      |
| medalion.png  | gaussian_filter  |          8'870           |     79     |
| half-life.png | gaussian_filter  |          94'409          |     8      |
| stars.png     | gaussian_filter  |        1'140'035         |     1      |
| medalion.png  | sobel_filter     |          15'834          |     44     |
| half-life.png | sobel_filter     |         105'979          |     7      |
| stars.png     | sobel_filter     |        1'291'380         |     1      |

###  Éviter les données null

Afin d'éviter de calculer lors qu'une des données est égal à 0 (puisque le résultat sera 0 de toutes manières), il a fallu ajouter un test dans les fonctions `gaussian_filter` et `sobel_filter`. Voici les performances pour 3 fonctions: 

| Images        | Fonction         | Temps de traitement [us] | Itérations |
| ------------- | ---------------- | :----------------------: | :--------: |
| medalion.png  | rgb_to_grayscale |          3'360           |    208     |
| half-life.png | rgb_to_grayscale |          12'060          |     58     |
| stars.png     | rgb_to_grayscale |          98'771          |     6      |
| medalion.png  | gaussian_filter  |          9'606           |     73     |
| half-life.png | gaussian_filter  |          19'819          |     36     |
| stars.png     | gaussian_filter  |         708'657          |     1      |
| medalion.png  | sobel_filter     |          18'436          |     38     |
| half-life.png | sobel_filter     |          27'703          |     25     |
| stars.png     | sobel_filter     |         826'024          |     1      |

Pour voir si cela a amélioré le nombre de fautes de page comparé au laboratoire précédent, il a fallu utilisé Perf et Valgrind. La même logique et la même présentation ont été appliquées ici. 

Pour Perf, cpu-clock est là comme indicateur et on regarde essentiellement les fautes de page.

Pour Valgrind, les fichiers .out sont ouverts à l'aide de kcachegrind et permettent de bien voir de façon graphique ce que consomme une fonction en terme de cache miss et autres. 

#### **Half-life**

###### Perf

cpu-clock 747

faults 170

| Fonction         | cpu-clock    | fault       |
| ---------------- | ------------ | ----------- |
| rgb_to_grayscale | 6.83% (~51)  | 7.91% (~13) |
| gaussian_filter  | 9.24% (~69)  | 7.27% (~13) |
| sobel_filter     | 12.72% (~95) | 7.79% (~13) |

Il y a eu 747 coups d'horloge, ce qui plus de 250 de moins qu'avant. Les fautes de page sont plus nombreuses par contre (~3 de plus). Ceci est peut-être dû que la machine faisait des choses en parallèle. Les endroits des fonctions qui posent problème sont toujours les accès mémoire après les calculs.

###### Valgrind

Le rapport pour cette image s'appelle "1_val_half-life.out"

##### rgb_to_grayscale

![image-20200422231014481](/home/oem/.config/Typora/typora-user-images/image-20200422231014481.png)

Au niveau des différents types de cache miss, les données sont pratiquement similaires au laboratoire précédent (à ~1%). Les remarques sont les mêmes

##### gaussian_filter

![image-20200422231827414](/home/oem/.config/Typora/typora-user-images/image-20200422231827414.png)

Même chose que pour la fonction précédente.

##### sobel_filter

![image-20200422230935769](/home/oem/.config/Typora/typora-user-images/image-20200422230935769.png)

Même chose que pour la fonction précédente.

#### **Medalion**

###### Perf

| Fonction         | cpu-clock    | fault      |
| ---------------- | ------------ | ---------- |
| rgb_to_grayscale | 4.58% (~14)  | 5.95% (~5) |
| gaussian_filter  | 12.75% (~39) | 5.91% (~5) |
| sobel_filter     | 25.16% (~77) | 5.67% (~5) |

Les 306 coups d'horloge représente une diminution de ~50 oups par rapport à avant. Le nombre de fautes de page reste plus au moins similaire pour les fonctions (sur les 57 présents, cela représente une diminution de moitié comparé à avant) et ce sont toujours mêmes endroits qui posent problème: les accès mémoire

###### Valgrind

Le rapport pour cette image s'appelle "2_val_medalion.out"

##### rgb_to_grayscale

![image-20200422232204916](/home/oem/.config/Typora/typora-user-images/image-20200422232204916.png)

Il y a une diminution de 10% des défauts de lecture en cache L3 à 38% au lieu de 48% sur l'ensemble du programme. Pourtant nous n'avons rien modifié dans cette fonction ce qui rend ce résultat bizarre. Un `make clean` n'a peut-être pas été fait.

##### gaussian_filter

![image-20200422232236560](/home/oem/.config/Typora/typora-user-images/image-20200422232236560.png)

Il y a eu plus de 20% de moins cache miss en écriture et lecture à la cache L1. A respectivement 44.64% et 43.82% sur l'ensemble des cache miss L1 du programme, cela représente une proportion très élevée, donc à améliorer.

##### sobel_filter

![image-20200422232309576](/home/oem/.config/Typora/typora-user-images/image-20200422232309576.png)

Je n'avais pas de données pour cette fonction dans le précédent laboratoire. Les pourcentages des cache miss de tous genres sont les que pour `gaussian_filter`. On pourrait dire que les mêmes remarques s'appliquent ici

#### **stars**

###### Perf

cpu-clock 15k

faults 2k

| Fonction         | cpu-clock      | fault         |
| ---------------- | -------------- | ------------- |
| rgb_to_grayscale | 2.82% (~423)   | 8.93% (~179)  |
| gaussian_filter  | 26.17% (~3926) | 8.95% (~179)  |
| sobel_filter     | 30.13% (~4520) | 17.19% (~344) |

Il a fallu 6'000 coups d'horloge de moins qu'avant (à ~15'000) pour exécuter le programme. Les proportions sont les mêmes. Pareil pour le nombre de fautes page. Par contre, `sobel_filter` a un nombre 2 fois plus élevé

###### Valgrind

Le rapport pour cette image s'appelle "3_val_stars.out"

##### rgb_to_grayscale

![image-20200422234220180](/home/oem/.config/Typora/typora-user-images/image-20200422234220180.png)

Il y a une grande diminution du nombre de cache miss L3 en lecture à 2.13% au lieu de 38.2%. 

`make clean` ?

Sinon c'est plus ou moins les même pourcentages

##### gaussian_filter

![image-20200422234250589](/home/oem/.config/Typora/typora-user-images/image-20200422234250589.png)

Même pourcentage que le laboratoire précédent pour les cache miss L1 mais une très grande augmentation des cache miss L3 à 46.4% en lecture et 43.79% en écriture au lieu de 3.83% et 6.03%

##### sobel_filter

![image-20200422234345918](/home/oem/.config/Typora/typora-user-images/image-20200422234345918.png)

Même constat que pour `gaussian_filter`.



Cette optimisation n'a pas beaucoup d'effet sur les fonctions si on considère les fautes de page. Elle permet de gagner du temps mais sans plus. Elle est même contre productive au vue des données de Valgrind. Plus rapide et moins de cache miss ? Les images de sortie doivent manquer des données. J'

### Inversion des boucles, 

Les boucles qui parcourt le tableau de données commencent par parcourir la largeur puis la hauteur:

![image-20200422160208586](/home/oem/.config/Typora/typora-user-images/image-20200422160208586.png)

Cela permet de parcourir une colonne de l'image après l'autre. Les données étant stockées en ligne de bloc, les accéder par colonne engendre plus d'appels mémoire (et de cache miss en lecture) car il faut charger à chaque fois un nouveau pour lire une donnée. En inversant les boucles, toutes les données d'un bloc seront traitées avant d'avoir un appel mémoire qui fait un cache miss. Voici le tableau des nouvelles performances:

| Images        | Fonction         | Temps de traitement [us] | Itérations |
| ------------- | ---------------- | :----------------------: | :--------: |
| medalion.png  | rgb_to_grayscale |          3'362           |    208     |
| half-life.png | rgb_to_grayscale |          12'064          |     58     |
| stars.png     | rgb_to_grayscale |          98'783          |     6      |
| medalion.png  | gaussian_filter  |          7'802           |     90     |
| half-life.png | gaussian_filter  |          11'490          |     61     |
| stars.png     | gaussian_filter  |         193'440          |     3      |
| medalion.png  | sobel_filter     |          17'282          |     40     |
| half-life.png | sobel_filter     |          20'379          |     34     |
| stars.png     | sobel_filter     |         356'950          |     2      |

Pour `rgb_to_grayscale`, il n'y a pas de changement. Cela est peut-être dû au fait que c'est la première fois que les données sont charger par le processeur. Pour les 2 autres, une petite amélioration (de 2'000 à 7'000 us) est visible pour les images half-life.png et medalion.png. L'image stars.png voit son temps diminuer de plus de 3.5 fois pour `gaussian_filter` et de plus de moitié pour `sobel_filter`. 



## Conclusion

Avec les modifications faites ci-dessus, les performances de `gaussian_filter` ont été améliorées d'un facteur 1.14, 8.2 et 5.9 et celles de `sobel_filter` d'un facteur 0.92, 5.2 et 3.6 pour medalion.png,  half-life.png et stars.png (à noter que les performances peuvent varier suivant ce que la machine est en train de faire en parallèle, ce qui expliquerait le facteur négatif de medalion.png à la fonction `sobel_filter`). `rgb_to_grayscale` ne rencontre aucune amélioration. C'est une chose que je peine à expliquer. L'inversion des boucles aurait dû, à mon sens avoir un effet mais ce n'est pas le cas

Il existe encore d'autres méthodes pour optimiser le code présent. Une des ces méthode serait de calculer en avance des pixels:

Pour calculer la convolution d'un pixel, il faut accéder aux 8 pixels qui l'entourent. Pour calculer celle du pixel suivant, il faudra accéder à 6 mêmes pixels (sur les 9 nécessaires) que pour celui d'avant. Cela permet de faire les calculs concernant pour le pixel futur en même temps que celui voulu. Ceci permet de diminuer les accès mémoire: ils passent de 9 à 3. Les derniers pixels d'une ligne sont traités différemment: le calcul de la dernière colonne (donc les 3 pixels inconnus) n'a pas besoin d'être fait, ce qui diminue encore les accès mémoire.

Je ne l'ai pas mise en place par manque de temps.