/* 26.01.16 - Xavier Ruppen - HPC - REDS - HEIG-VD */

#include <stdio.h>
#include <stdlib.h>
#include "benchmark/benchmark.h"
#include "bench.h"
#include        <chrono>

extern "C" {
#include "matrix.h"
#include "array_util.h"
#include "list_util.h"

#define DATA_LEN 100000
}

class MatrixFixture : public benchmark::Fixture {
public :
    void SetUp(const ::benchmark::State&)
    {
        m1 = matrix_init(MATRIX1_ROW, MATRIX1_COL);
        m2 = matrix_init(MATRIX2_ROW, MATRIX2_COL);
        m3 = matrix_init(MATRIX1_ROW, MATRIX2_COL);
    }

    void TearDown(const ::benchmark::State&)
    {
        matrix_clear(m1);
        matrix_clear(m2);
        matrix_clear(m3);
    }

protected :
    struct matrix *m1, *m2, *m3;
};

class ArrayFixture : public benchmark::Fixture {
public :
    void SetUp(const ::benchmark::State&)
    {
      array = array_init(DATA_LEN);
    }

    void TearDown(const ::benchmark::State&)
    {
        array_clear(array);
    }

protected :
      uint64_t *array;
};

class ListFixture : public benchmark::Fixture {
public :
    void SetUp(const ::benchmark::State&)
    {
      list = list_init(DATA_LEN);
    }

    void TearDown(const ::benchmark::State&)
    {
        list_clear(list);
    }

protected :
      struct list_element *list;
};

/*BENCHMARK_F(MatrixFixture, matrix_mult)(benchmark::State& state) {
    while (state.KeepRunning()) {
        matrix_zero(m3);

        if (!matrix_mult(m1, m2, m3)) {
            fprintf(stderr, "[%s] wrong matrix sizes\n", __func__);
            exit(EXIT_FAILURE);
        }
    }
}

BENCHMARK_F(MatrixFixture, matrix_mult_xchg)(benchmark::State& state) {
    while (state.KeepRunning()) {
        matrix_zero(m3);

        if (!matrix_mult_xchg(m1, m2, m3)) {
            fprintf(stderr, "[%s] wrong matrix sizes\n", __func__);
            exit(EXIT_FAILURE);
        }
    }
}*/

BENCHMARK_DEFINE_F(ArrayFixture, array_sort)(benchmark::State& state) {
    while (state.KeepRunning()) {
        array_sort(array, DATA_LEN);
    }
}

BENCHMARK_DEFINE_F(ListFixture, list_sort)(benchmark::State& state) {
    while (state.KeepRunning()) {
        list_sort(list);
    }
}

// Resgistre the benchmark and say how many time it must be repeated
BENCHMARK_REGISTER_F(ArrayFixture,array_sort)->Repetitions(10);
BENCHMARK_REGISTER_F(ListFixture,list_sort)->Repetitions(10);

BENCHMARK_MAIN();
