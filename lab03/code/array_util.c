#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>

#include "array_util.h"

#define NB_MAX 100

uint64_t *array_init(const size_t len){
	uint64_t *array = malloc(len * sizeof(uint64_t));

	// Si l'allocation a échoué
	if (!array){
		exit(EXIT_FAILURE);
	}

	// Initialise les nombres aléatoires
	srand(time(NULL));

	// Remplit les cellules de nombres aléatoires (0 à 100)
	for (size_t cell = 0; cell < len; ++cell){
	array[cell] = rand() % NB_MAX;
	}

	return array;
}

void array_clear(uint64_t *data){
	free(data);
}

/*
 * Swap two values in an array
 */
void swapVal(uint64_t *xp, uint64_t *yp)
{
    uint64_t temp = *xp;
    *xp = *yp;
    *yp = temp;
}

/*
 * Bubble sort
 */
void bubbleSortArray(uint64_t arr[], size_t n)
{
   size_t i;
	 size_t j;

   for (i = 0; i < n-1; i++){
       // Last i elements are already in place
       for (j = 0; j < n-i-1; j++){
           if (arr[j] > arr[j+1]){
              swapVal(&arr[j], &arr[j+1]);
					 }
			 }
	 }
}

void array_sort(uint64_t *data, const size_t len){
		bubbleSortArray(data, len);
}

void displayArray (uint64_t *array, const size_t len){
	// Affichage le tableau
	for (size_t cell = 0; cell < len; ++cell){
		printf("%" PRIu64 " ", array[cell]);
	}
	printf("\n");
}
