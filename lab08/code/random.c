/* 06.05.2020 - Sydney Hauke - HPC - REDS - HEIG-VD */
#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>
#include <tmmintrin.h>
#include <smmintrin.h>
#include <nmmintrin.h>
#include <ammintrin.h>
#include <immintrin.h>

#include "random.h"

void xorshift32(struct xorshift32_states *states){

  __m128i x = _mm_loadu_si128(states->a);
  __m128i xtmp = x;

  // x ^= x << 13
  xtmp = _mm_slli_epi32(xtmp, 13);
  x = _mm_xor_si128(x, xtmp);
  xtmp = x;

  // x ^= x >> 17
  xtmp = _mm_srli_epi32(xtmp, 17);
  x = _mm_xor_si128(x, xtmp);
  xtmp = x;

  // x ^= x << 5
  xtmp = _mm_slli_epi32(xtmp, 5);
  x = _mm_xor_si128(x, xtmp);

  _mm_store_si128(states->a, x);
}
