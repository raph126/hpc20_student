#pragma once

#include <stdint.h>

#define IMG_TMP             4

struct xorshift32_states {
    uint32_t a[IMG_TMP];
};

void xorshift32(struct xorshift32_states *states);
