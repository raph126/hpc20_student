# HPC - Perf & Valgrind

## Introduction

Pour ce laboratoire, il nous a été demandé de se familiariser avec les outils `perf` et `valgrind` à l'aide de deux tutoriaux. Pour chacun de ces outils, j'ai écrit un résumé du tutorial ainsi qu'un exemple d'utilisation sur la fonction `array_sort` du labo 1. Ensuite j'ai refait la même démarche mais pour `list_sort`. La taille des structures est la même que pour le laboratoire précédent pour PERF: 100'000. Pour Valgrind, j'ai dû la reduire à 10'000

## PERF

Perf, Performance Events for Linux, est un outil de profiling du kernel Linux (disponible depuis Linux 2.6). Cet outil est composé de deux parties: un kernel SYSCALL afin d'avoir accès aux données de performance du système et du matériel, ainsi qu'une partie dans le user-space pour collecter, analyser et afficher les données. Il faut installer `linux-tools` pour disposer de PERF. Son exécution doit se faire en administrateur. Sinon, il ne peut pas collecter les données.

Pour voir les événements, hardware  et software que PERF peut mesurer, il faut taper `perf list`. Cela retourne une longue liste. Les plus utilisés pour le hardware sont :

```
cpu-cycles OR cycles
instructions
cache-references
cache-misses
branch-instructions OR branches
branch-misses
bus-cycles
stalled-cycles-frontend OR idle-cycles-frontend
stalled-cycles-backend OR idle-cycles-backend
ref-cycles
```

et pour le software:

```
cpu-clock
task-clock
page-faults OR faults
context-switches OR cs
cpu-migrations OR migrations
minor-faults
major-faults
alignment-faults
emulation-faults
```

Exemples simples:

Il est possible de mesurer un seul événement

`perf stat -e cpu-cycles ./sort array 10000`

ou bien plusieurs. Dans ce cas, ils doivent être séparés par une virgule

```
perf stat -e cpu-clock,cycles ./sort array 10000
perf stat -e cs,migrations,faults ./sort list 10000
```

### Qu'est-ce que je me dois demandé ?

- **Quelles parties du programme prennent le plus de temps ?**

  Les parties du code qui prennent le plus de temps sont appelées *hot spots*. Ces hot spots sont les parties où on peut optimiser avec peu d'effort (en général). Ils se trouvent facile avec `cpu-clock`.

- **Est-ce que le nombre d'événements hardware ou software indique t-il un problème réel de performance pouvant être résolu ?**

  Pour déterminer si il y a un problème, une connaissance des algos et des structures du programme est requise. Il faut au moins savoir si il est lié au CPU (effectue beaucoup de calculs) ou à la mémoire (doit lire/écrire beaucoup de données en mémoire) et où les événements se produisent dans le programme. Si ils se produit dans un hot spot, il est très probable que c'est un réel problème de performance à résoudre. (événement à regarder : page fault)

- **Comment je peux corriger ce problème de performance ?**

  Il faut étudier les structures et les algos du programme afin de voir si sa charge de travail élevée peut être réduite. Une réorganisation ou une réduction des données pour réduire le nombre de pages mémoire. Après avoir modifié le programme, il faut encore comparer la performance du nouveau code avec l'ancien pour savoir lequel est le meilleur. 

  Une hypothèse peut être fausse. Il ne faut pas en être surpris. Il faut continuer à essayer

  

### Comment trouver les hot spots ?

PERF dispose d'un outil pour faciliter la recherche de hot spots: les *profiles*. Un profile est un tableau ou du code annoté qui montre le temps d'exécution des points chauds et/ou  les régions du code où un événements hard/software a lieu. Il faut procéder en deux étapes:

- l'exécution du programme et la collecte des données pour le profile
- l'affichage des données et l'analyse du profile

La première étape est faite avec la commande `perf record`. Elle lance le programme pour la collecte des données du profile. Ces données sont sauver dans un fichier perf.data . La seconde utilise les commandes `perf report`et `perf annotate` . Ces deux commandes permettent d'explorer les données du profile. Il faut commencer par les hot spots les plus importants avant de passer aux autres.

### Exemple

Afin d'illustrer la démarche, j'ai pris comme exemple la fonction `array_sort` du laboratoire 1

La récolte des données s'est faite avec la commande `perf record` afin de mesurer le nombre de coup d'horloge et le nombre de page faults du programme:

```
perf record -e cpu-clock,faults ./sort array 100000
[ perf record: Woken up 16 times to write data ]
[ perf record: Captured and wrote 3.952 MB perf.data (85903 samples) ]
```

Pour afficher le profile, il y a deux choix à disposition:

- l'affichage standard de la console (TUI) avec `perf report`. Cela va ouvrir un menu dans l'interface GTK2 avec les différentes mesures demandées et on peut naviguer dedans avec les flèches du claviers (avec l'option `--gtk`, il se passe la même chose)
- avec l'option `--stdio`. Cela va sortir le rapport directement dans la console

Si on affiche le profile avec le premier choix, on obtient ce menu:

```
Available samples
85K cpu-clock
15 faults      
```

Il a fallu environ 85'000 coups d'horloge pour exécuté le programme avec 15 fautes de page. On peut sélectionner une mesure afin de voir quels sont les facteurs qui ont émis le plus d'événement. Dans le cas de `cpu-clock`, c'est la fonction array_sort qui est en première avec 99.78% des coups. Pour les fautes de page, array_init vient en première avec 72.33%. Si on sélectionne une de ces fonctions, on obtient le menu suvant:

```
Annotate array_sort  
Zoom into sort thread
Zoom into sort DSO
Browse map details
Run scripts for samples of symbol [array_sort]
Run scripts for all samples
Switch to another data file in PWD
Exit        
```

A partir de là, on peut naviguer dans les différents menus pour voir ce qui produit le plus d'événements ("Annotate array_sort" montre le code désassemblé).

Pour array_sort, il est normal que cette fonction prend le plus de coups d'horloge: c'est le cœur du programme. Par contre, le tri à bulle n'est pas réputé pour être le plus efficace. En change d'algorithme pour un tri rapide ou par tas (si assez de mémoire), il se peut qu'on puisse optimiser cette partie du code. Dans le code désassemblé, on voit qu'il y a plus de 68% du temps de la fonction qui se passe sur le swap des valeurs. Au lieu de changer les valeurs entières, on pourrait changer les pointeurs de place dans le tableau (à tester).

Pour array_init, c'est à ce moment que le programme va écrire dans la mémoire afin d'initialiser le tableau de données. Dans le code désassemblé, il est très clair que c'est l'écriture dans le tableau qui prend tout le temps

![image-20200401210337452](/home/oem/.config/Typora/typora-user-images/image-20200401210337452.png)

Il faudrait comparer avec list_init pour voir si un changement de structure de données permet une amélioration ou bien si cela ne change rien.

## Valgrind

Valgrind est un autre outil de profiling. Pour l'installer, il suffit de taper la commande `apt install valgrind`. Il est connu pour la fonction memcheck. Elle permet de détecter les fuites et erreurs mémoire. La de Valgrind qui nous intéresse pour ce laboratoire est callgrind. Elle permet de créer des profiles pour mesurer la performance CPU/cache.

Pour lancer callgrind, il suffit de taper `valgrind --tool=callgrind ./sort array 10000` dans la console. Le programme va s'exécuter plus lentement à cause qu Valgrind va intervenir dans le processus. Il va retourner un bref sommaire qui montre combien d'événements ont été collectés. 

```
valgrind --tool=callgrind ./sort array 10000
==8545== Callgrind, a call-graph generating cache profiler
==8545== Copyright (C) 2002-2017, and GNU GPL'd, by Josef Weidendorfer et al.
==8545== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==8545== Command: ./sort array 10000
==8545== 
==8545== For interactive control, run 'callgrind_control -h'.
==8545== 
==8545== Events    : Ir
==8545== Collected : 400259024
==8545== 
==8545== I   refs:      400,259,024
```

 Le tout a été enregistré dans un ficher nommé `callgrind.out.8545` (à ouvrir avec `callgrind_annotate --auto=yes callgrind.out.8545` dans un terminal car c'est illisible ici).

Cela va nous donne uniquement le résultat pour un type d'événement par fonction: `Ir` . Mais il y en a d'autres :

- `Ir`: I cache reads (instructions executed)
- `I1mr`: I1 cache read misses (instruction wasn't in I1 cache but was in L2)
- `I2mr`: L2 cache instruction read misses (instruction wasn't in I1 or L2 cache, had to be fetched from memory)
- `Dr`: D cache reads (memory reads)
- `D1mr`: D1 cache read misses (data location not in D1 cache, but in L2)
- `D2mr`: L2 cache data read misses (location not in D1 or L2)
- `Dw`: D cache writes (memory writes)
- `D1mw`: D1 cache write misses (location not in D1 cache, but in L2)
- `D2mw`: L2 cache data write misses (location not in D1 or L2)

Pour mesurer tous les types d'événements, il faut rajouter le flag `--simulate-cache=yes` avant le nom du programme. 

```
valgrind --tool=callgrind --simulate-cache=yes ./sort array 10000
==8607== Callgrind, a call-graph generating cache profiler
==8607== Copyright (C) 2002-2017, and GNU GPL'd, by Josef Weidendorfer et al.
==8607== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==8607== Command: ./sort array 10000
==8607== 
--8607-- warning: L3 cache found, using its data for the LL simulation.
==8607== For interactive control, run 'callgrind_control -h'.
==8607== 
==8607== Process terminating with default action of signal 27 (SIGPROF)
==8607==    at 0x4D3BDAF: __open_nocancel (open64.c:69)
==8607==    by 0x4D4F91F: write_gmon (gmon.c:370)
==8607==    by 0x4D500DA: _mcleanup (gmon.c:444)
==8607==    by 0x4C6F040: __run_exit_handlers (exit.c:108)
==8607==    by 0x4C6F139: exit (exit.c:139)
==8607==    by 0x4C4DB9D: (below main) (libc-start.c:344)
==8607== 
==8607== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==8607== Collected : 400234799 100210174 49433591 1017 5087902 2377 1005 2053 1874
==8607== 
==8607== I   refs:      400,234,799
==8607== I1  misses:          1,017
==8607== LLi misses:          1,005
==8607== I1  miss rate:        0.00%
==8607== LLi miss rate:        0.00%
==8607== 
==8607== D   refs:      149,643,765  (100,210,174 rd + 49,433,591 wr)
==8607== D1  misses:      5,090,279  (  5,087,902 rd +      2,377 wr)
==8607== LLd misses:          3,927  (      2,053 rd +      1,874 wr)
==8607== D1  miss rate:         3.4% (        5.1%   +        0.0%  )
==8607== LLd miss rate:         0.0% (        0.0%   +        0.0%  )
==8607== 
==8607== LL refs:         5,091,296  (  5,088,919 rd +      2,377 wr)
==8607== LL misses:           4,932  (      3,058 rd +      1,874 wr)
==8607== LL miss rate:          0.0% (        0.0%   +        0.0%  )
Expiration de la minuterie durant l'établissement du profile
```

L'outil s'est arrêté en cours de route (certainement à cause de la taille de la structure qui est trop grande). Mais on peut déjà remarquer des tendances sur ce qui a été mesuré. (Fichier `callgrind.out.8607`, à ouvrir avec `callgrind_annotate --auto=yes callgrind.out.8607` dans un terminal car c'est illisible ici).

Avec ce qui a été annoté, on voit qu'il y a beaucoup de cas où le programme doit aller chercher dans la mémoire car les données ne sont pas dans les caches. De plus, les boucles `for` demandent beaucoup de cycles d'horloges. Peut-être en les changeant, nous pouvons gagner un peu de performance (pour une de ces boucles, la CPU doit aller chercher plus de 5'000'000 de fois dans la cache L2 au minimum).

### Lecture des résultats

Pour les `Ir`, il faut faire attention à la lecture des nombres affichés. Valgrind va compter toutes les instructions assembleur. Une ligne de code C peut compter plusieurs instructions assembleur. Call_annotate montre les fonctions les plus coûteuses avant d'afficher le code.



## list_sort & array_sort

### array_sort

Voir chapitres précédents.

### list_sort

#### Perf

Pour la liste chaînée, il a fallu environ 96'000 coups d'horloge et 13 fautes de page pour la trier. Pour cpu-clock, c'est list_sort qui prend le plus temps et pour les fautes, c'est le malloc de l'initialisation qui en produit le plus. Comme pour array_init, c'est quand on modifie la mémoire qu'on obtient des fautes de page. Le nombre n'est pas si élevé des 2 cas, je pense qu'il n'est pas nécessaire de faire une modification.

Pour list_array, la majorité du temps est passé à déréférencer des pointeurs (~43%) et aux comparaisons des données (~35%). La liste chaîné n'est pas plus optimisée qu'un tableaux. Encore une fois, le tri à bulle n'est pas le meilleur tri. Il existe des tris ayant moins de comparaisons pouvant faire gagner beaucoup de temps (donc demandant moins de pointeurs déréférencés).

![image-20200401213933192](/home/oem/.config/Typora/typora-user-images/image-20200401213933192.png)



#### Valgrind

Pour Valgrind, j'ai tout de suite mesuré tous les événements

```
valgrind --tool=callgrind --simulate-cache=yes ./sort list 10000
==9616== Callgrind, a call-graph generating cache profiler
==9616== Copyright (C) 2002-2017, and GNU GPL'd, by Josef Weidendorfer et al.
==9616== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==9616== Command: ./sort list 10000
==9616== 
--9616-- warning: L3 cache found, using its data for the LL simulation.
==9616== For interactive control, run 'callgrind_control -h'.
==9616== 
==9616== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==9616== Collected : 478098057 150740608 50043771 1102 24723836 6237 1084 2063 5624
==9616== 
==9616== I   refs:      478,098,057
==9616== I1  misses:          1,102
==9616== LLi misses:          1,084
==9616== I1  miss rate:        0.00%
==9616== LLi miss rate:        0.00%
==9616== 
==9616== D   refs:      200,784,379  (150,740,608 rd + 50,043,771 wr)
==9616== D1  misses:     24,730,073  ( 24,723,836 rd +      6,237 wr)
==9616== LLd misses:          7,687  (      2,063 rd +      5,624 wr)
==9616== D1  miss rate:        12.3% (       16.4%   +        0.0%  )
==9616== LLd miss rate:         0.0% (        0.0%   +        0.0%  )
==9616== 
==9616== LL refs:        24,731,175  ( 24,724,938 rd +      6,237 wr)
==9616== LL misses:           8,771  (      3,147 rd +      5,624 wr)
==9616== LL miss rate:          0.0% (        0.0%   +        0.0%  )
```

La lecture du fichier se fait avec la commande `callgrind_annotate --auto=yes callgrind.out.9616`

La boucle `while` effectue moins d'instructions que les boucles `for` utilisées dans pour array_sort. Par contre nous avons encore plus de cache miss en L2. Le fait qu'une liste chaînée prend plus de place en mémoire peut en être la cause (elle ne pourrait être en entière dans la cache L1). Une structure moins lourde (un tableau) serait mieux dans ce cas-là.







