#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>

#include "list_util.h"

#define NB_MAX 100

// https://www.geeksforgeeks.org/quicksort-on-singly-linked-list/
// https://stackoverflow.com/questions/35103855/c-quicksort-linked-list

struct list_element{
	uint64_t val;
	struct list_element *next;
};

/* Add a new element in the beginning of the list */
void push(struct list_element** head_ref, int new_data)
{
    /* allocate node */
    struct list_element* elem =  malloc(sizeof(struct list_element));

		// allocation failed
		if (!elem){
			exit(EXIT_FAILURE);
		}

    /* put in the data */
    elem->val = new_data;

    /* link the old list off the new node */
    elem->next = (*head_ref);

    /* move the head to point to the new node */
    (*head_ref) = elem;
}

struct list_element *list_init(size_t len){
		struct list_element *list = NULL;

		// Initialise les nombres aléatoires
		srand(time(NULL));

		// Crée le nombre d'éléments avec une valeurs aléatoires (0 à 100)
		for (size_t cpt = 0; cpt < len; ++cpt){
				push(&list, rand() % NB_MAX);
		}

		return list;
}

void list_clear(struct list_element *head){
		struct list_element *oldHead;

		while (head->next != NULL){
			oldHead = head;
			head = head->next;
			free(oldHead);
		}
}

/*
 * Swap the value of two elements a and b
 */
void swapValElem(struct list_element *a, struct list_element *b)
{
    uint64_t temp = a->val;
    a->val = b->val;
    b->val = temp;
}

/*
 * Bubble sort the given linked list
 */
void bubbleSortList(struct list_element *start)
{
		// vars temporaires afin de bien séparer les opérations
		// (surtout pour le code désassemblé)
		uint64_t v1;
		uint64_t v2;

    uint64_t swapped;
    struct list_element *ptr1;
    struct list_element *lptr = NULL;

    // Checking for empty list
    if(start == NULL){
        return;
		}

    do
    {
        swapped = 0;
        ptr1 = start;

        while (ptr1->next != lptr)
        {
						v1 = ptr1->val;
						v2 = ptr1->next->val;

            if (v1 > v2)
            {
                swapValElem(ptr1, ptr1->next);
                swapped = 1;
            }
            ptr1 = ptr1->next;
        }
        lptr = ptr1;
    }
    while (swapped);
}

void list_sort(struct list_element *head){
		bubbleSortList(head);
}

void displayList (struct list_element *list){
		while (list != NULL)
	  {
	      printf("%" PRIu64 " ", list->val);
	      list = list->next;
	  }
	  printf("\n");
}
